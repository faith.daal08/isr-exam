import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() searchModel: any;

  @Output() searchModelChange: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  updateSearchModel(value: any) {
    this.searchModel = value;
    this.searchModelChange.emit(this.searchModel);
  }
}
