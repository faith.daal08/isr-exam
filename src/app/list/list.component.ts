import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  projects: any;
  searchModel: any;
  commits: any
  commitMsg: string = ''
  // projectId: any;

  @Output() public select: EventEmitter<{}> = new EventEmitter();

  constructor(private http: HttpClient) {}


  ngOnInit() {
    this.signIn();
    // this.getLastCommit() 
  }

  signIn() {
    return this.http.get(`https://gitlab.com/api/v4/projects?private_token=fQN1dcosGGFy_p6MpBpx`).subscribe(data => {
     this.projects = data;
     console.log(this.projects, 'prpjects')
  })
  }
  getLastCommit(projectId: number, index: number) {
    return this.http.get('https://gitlab.com/api/v4/projects/' + projectId +'/repository/commits').subscribe(data => {
      this.commits = data;
      console.log(this.commits, 'commits')
      if (this.commits.length == 0 || this.commits == []) this.commitMsg = ''
      else {
      let count: number = 0
      this.commits.forEach((element: any, i: number) => {
        this.projects.forEach((project: any, idx: number) => {
          if (index === idx && i == this.commits.length-1) {
            project.lastCommit = element.message
          }
        })
        // if (i == this.commits.length-1)  this.commitMsg = element.message
        // else this.commitMsg = ''
      })
    }
   })

  }

  getCommit() {
    console.log(this.commits)
    // if (this.commits.length == 0 || this.commits == []) this.commitMsg = ''
    // else {
    //   let count: number = 0
    //   this.commits.forEach((element: any, i: number) => {
    //     console.log(element,i, 'elemmetn')
    //     this.commitMsg = 'more than 1 commit'
    //   })
    // }
  }
}
